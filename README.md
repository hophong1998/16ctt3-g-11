# README #

Đây là Remote Repository chung của nhóm 16CTT3-G11

## Thông tin các thành viên ##

### Project Manager ###
* Họ tên: Trương Hổ Phong
* MSSV: 1612506
* SĐT: 01655729988
* Email: 1612506@student.hcmus.edu.vn

### Technical Leader ###
* Họ tên: Hứa Nguyên Bảo
* MSSV: 1512024
* SĐT: 0164 503 745
* Email: 1512024@student.hcmus.edu.vn

### Tester (QC) ###
* Họ tên: Phan Quốc Phong
* MSSV: 1612498
* SĐT: 0969 545 433
* Email: 1612498@student.hcmus.edu.vn

### Technical Writer ###
* Họ tên: Tạ Thị Tú Phi
* MSSV: 1612496
* SĐT: 01634 994 998
* Email: 1612496@student.hcmus.edu.vn

### Dev 1 ###
* Họ tên: Huỳnh Hồng Ân
* MSSV: 1512014
* SĐT: 0981864150
* Email: 1512014@student.hcmus.edu.vn

### Dev 2 ###
* Họ tên: Nguyễn Quang Phú
* MSSV: 1612508
* SĐT: 0947421123
* Email: 1612508@student.hcmus.edu.vn
