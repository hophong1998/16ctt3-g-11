﻿#include "Polynom.h"
#include"Monomial.h"
#include"Queue.h"

// constructor
Polynom::Polynom()
{
	pHead = pTail = NULL;
}

// constructor
Polynom::Polynom(const Polynom& x)
{
	this->pHead = NULL;
	for (auto i = x.pHead; i; i = i->pNext)
	{
		this->addTail(i->data);
	}
}

// constructor
Polynom& Polynom::operator=(const Polynom& x)
{
	this->pHead = NULL;
	for (auto i = x.pHead; i; i = i->pNext)
	{
		this->addTail(i->data);
	}
	return *this;
}

//destructor
Polynom::~Polynom()
{
	if (pHead == pTail)  // đa thức có 1 đơn thức
	{
		delete pHead;
		pHead = NULL;
	}
	else
	{
		for (Mono* p = pHead; p; p = pHead) {
			pHead = pHead->pNext;
			delete p;
		}
	}
}

// tạo một node có data là đơn thức
Mono* Polynom::getMono(Monomial dt) {
	Mono* p = new Mono;
	if (p == NULL) return NULL;
	p->data = dt;
	p->pNext = NULL;
	p->pPrev = NULL;
	return p;
}

// thêm node đơn thức vào đa thức
bool Polynom::addTail(Monomial dt) {
	Mono* p = getMono(dt);
	if (p == NULL) return false;  // không đủ bộ nhớ
	if (pHead == NULL)
		pHead = pTail = p;
	else
	{
		p->pPrev = pTail;
		pTail->pNext = p;
		pTail = p;
		pTail->pNext = NULL;
	}
	return true;
}

// chuẩn hóa đa thức: sắp xếp 
void Polynom::Normalization() {
	for (Mono* p = pHead; p; p = p->pNext) {
		p->data.Simplify();

		// nếu biến là a và số mũ là 1 thì không hoán vị xét đơn thức kế tiếp
		if (p->data.var_num == 0 || p->data.index[0] == 1 && p->data.var[0] == 'a')
			continue;

		for (Mono* q = p->pNext; p != pTail, q; q = q->pNext) {
			//int mQ = q->data.getMaxIndex();
			q->data.Simplify();
			if (p->data.sortMonomial(q->data) == false) {
				swap(p->data, q->data);
			}
		}
		p->data.Simplify();

		//----------------------------------
		if (p->data.getCoeff() == 0) {
			if (p == pTail) {
				pTail = pTail->pPrev;
				pTail->pNext = NULL;
				delete p;
				break;
			}
			else {
				Mono* q = p->pNext;

				p->data = q->data;
				p->pNext = q->pNext;
				q->pNext->pPrev = p;
				delete q;
				p = p->pPrev;

				// sau khi xóa thì p=p->prev;
			}
		}

		//---------------------------------------------
	}
}

void Polynom::Simplify(){
	if (this->pHead != NULL) {
		this->Normalization();
		for (Mono* p = pHead; p != pTail; p = p->pNext) {
			Mono* q = p->pNext;
			if (p->data == q->data) {
				p->data.coeff += q->data.coeff; // cộng hệ số lại
				if (p->data.coeff == 0) { // sau khi cộng dồn hệ số = 0, xóa 2 node vừa cộng
					if (q == pTail) {
						pTail = p->pPrev;
						pTail->pNext = NULL;
						delete p;
						delete q;
						return;
					}
					else if (p == pHead) {
						pHead = q->pNext;
						pHead->pPrev = q;

					}
					else {
						Mono* r = p;
						p = p->pPrev;
						p->pNext = q->pNext;
						q->pNext->pPrev = p;
						delete r;
						delete q;
					}
				}
				else { // sau khi cộng hệ số khác 0, xóa node phía sau
					if (q == pTail) {
						pTail = p;
						pTail->pNext = NULL;
						delete q;
						return;
					}
					else if (p == pHead) {
						q->data.coeff = p->data.coeff;
						pHead = q;
					}
					else {
						p->pNext = q->pNext;
						q->pNext->pPrev = p;
						delete q;
						p = p->pPrev;
					}
				}
			}
		}
		while (pHead->pPrev != NULL) {
			Mono *p = pHead->pPrev;
			pHead->pPrev = p->pPrev;
			delete p;
		}
	}
}


// phân tích đa thức
Polynom& Polynom::ParsePolynom(string s) {
	QUEUE<string> Q(100);
	string sep = "+-";
	int fromPos = 0; //  tìm vị trí đầu tiên khác +-
	int toPos = s.find_first_of(sep, fromPos + 1); // tìm vị trí đầu tiên +-, trả về -1 nếu không tìm thấy
	int len = toPos - fromPos; // độ dài của một đơn thức
	while (len>0) {
		string a = s.substr(fromPos, len);
		Q.enQueue(a); // thêm chuỗi đơn thức vừa tách ra vào queue
		a.clear();
		fromPos = toPos;
		toPos = s.find_first_of(sep, fromPos + 1);
		len = toPos - fromPos;
	}
	while (!Q.isEmpty()) {
		Monomial dt;
		string a = Q.frontValue();
		char* temp = new char[a.length() + 1];
		for (int i = 0; i < a.length(); i++)
			temp[i] = a[i];
		dt.ParseMonomial(temp, a.length());  // phân tích đơn thức
		this->addTail(dt); // thêm đơn thức vừa tách vào đa thức
		Q.deQueue();
	}
	return *this;
}

// nhân đa thức với số nguyên, operator *=
Polynom& Polynom::operator*=(float x)
{
	for (Mono*p = pHead; p; p = p->pNext)
	{
		p->data.coeff *= x;
	}
	return *this;
}

// nhân đa thức với số nguyên, operator *
Polynom Polynom::operator*(float x)
{
	Polynom R(*this);
	for (Mono*p = R.pHead; p; p = p->pNext)
	{
		p->data.coeff *= x;
	}
	return R;
}

//operator += đa với đa
Polynom&  Polynom::operator+=(const Polynom &T)
{
	if (this == &T)
	{
		*this *= 2;
		return *this;
	}
	// Lần lượt thêm các đơn thức của đa thức T vào đa thức đối tượng
	for (Mono* p = T.pHead; p; p = p->pNext) {
		this->addTail(p->data);
	}
	this->Simplify();	// Đơn giản hóa đa thức
	return *this;
}

// operator+ đa với đa
Polynom  Polynom::operator+(const Polynom &T)
{
	Polynom R(*this); // copy this vào R;

	// Lần lượt thêm các đơn thức đa thức T vào đa thức R
	for (Mono* p = T.pHead; p; p = p->pNext) {
		R.addTail(p->data);
	}
	R.Simplify();	// Đơn giản hóa đa thức
	return R;
}

// operator += đa với đơn
Polynom& Polynom::operator+=(const Monomial &T)
{
	this->addTail(T);	// Thêm đơn thức cần tính tổng vào đa thức mới
	this->Simplify();	// Đơn giản hóa đa thức
	return *this;
}

// operator + đa với đơn
Polynom Polynom::operator+(const Monomial &T)
{
	Polynom R(*this);
	// Lần lượt thêm các đơn thức của đa thức cần tính tổng vào đa thức mới
	R.addTail(T);	// Thêm đơn thức cần tính tổng vào đa thức mới
	R.Simplify();	// Đơn giản hóa đa thức
	return R;
}

// operator-= đa với đa
Polynom& Polynom::operator-=(const Polynom& T)
{
	if (this == &T) {
		this->~Polynom();
		return *this;
	}
	Polynom R(T);
	*this += R*(-1);
	this->Simplify();
	return *this;
}

// operator- đa với đa
Polynom Polynom::operator-(const Polynom& T)
{
	if (this == &T) {
		this->~Polynom();
		return *this;
	}
	Polynom Q(T);
	Polynom R(*this);
	R += Q*(-1);
	this->Simplify();
	return R;
}

// operator-= đa với đơn
Polynom& Polynom::operator-=(Monomial x)
{
	x = x*(-1);
	this->addTail(x);
	this->Simplify();
	return *this;
}

// operator- đa với đơn
Polynom Polynom::operator-(Monomial x)
{
	Polynom R(*this);
	x = x*(-1);
	R.addTail(x);
	R.Simplify();
	return R;
}

//ghi file
void Polynom::Output(ostream& oDev){
	Mono* p = pHead;
	if (p == NULL)  // đa thức không có gì
	{
		oDev << "0";
		return;
	}
	oDev << p->data;
	for (p = pHead->pNext; p; p = p->pNext) {
		if (p->data.getCoeff() >= 0) {
			oDev << " +" << p->data;
		}
		else {
			oDev << " " << p->data;
		}
	}
	oDev << "\n";
}

//operator <<
ostream& operator<<(ostream& oDev, Polynom& DT) {
	Mono* p = DT.pHead;
	if (p == NULL)  // đa thức không có gì
	{
		oDev << "0";
		return oDev;
	}
	oDev << p->data;
	for (p = DT.pHead->pNext; p; p = p->pNext) {
		if (p->data.getCoeff() >= 0) {
			oDev << " +" << p->data;
		}
		else {
			oDev << " " << p->data;
		}
	}
	return oDev;
}

//--------------------------------------------------
//operator * đa thức với đơn thức
Polynom Polynom::operator*(const Monomial &T){
	Polynom R(*this);
	for (Mono* p = R.pHead; p; p = p->pNext) {
		p->data = p->data* T;
	}
	R.Simplify();	// Đơn giản hóa đa thức
	return R;
}

//operator *= đa thức với đơn thức
Polynom& Polynom::operator*=(const Monomial &T){

	for (Mono* p = pHead; p; p = p->pNext) {
		p->data = p->data* T;
	}
	this->Simplify();	// Đơn giản hóa đa thức
	return *this;
}

//operator * đa thức với đa thức
Polynom Polynom::operator*(const Polynom &T){
	Polynom R;
	for (Mono *p = T.pHead; p; p = p->pNext) {
		R += (*this*p->data);
	}
	R.Simplify();	// Đơn giản hóa đa thức
	return R;
}

//operator *= đa thức với đa thức
Polynom& Polynom::operator*=(const Polynom &T){
	Polynom R;
	for (Mono *p = T.pHead; p; p = p->pNext) {
		R += (*this*p->data);
	}
	R.Simplify();	// Đơn giản hóa đa thức
	*this = R;
	return *this;
}