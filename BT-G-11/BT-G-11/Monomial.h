﻿#pragma once
#include<iostream>
#include<algorithm>

using namespace std;

class Monomial // đơn thức
{
	float coeff; // hệ số
	float* index; // danh sách số mũ tương ứng với biến
	char* var; // danh sách các biến
	unsigned char var_num; // số lượng biến
public:
	Monomial();
	Monomial(const Monomial&);
	~Monomial();

	// lấy hệ số
	float getCoeff() {
		return coeff;
	}

	void Normalization(); // chuẩn hóa
	void Simplify(); // rút gọn
	void Output(ostream& oDev);

	Monomial& operator*(float); // nhân đơn thức với 1 số.
	Monomial operator*(const Monomial&); // nhân đơn thức với đơn thức.
	Monomial& operator*=(const Monomial&); // nhân đơn thức với đơn thức.

	Monomial& operator=(const Monomial&); // operator =
	Monomial& ParseMonomial(char* s, int len); // phân tích đơn thức.

	bool operator==(const Monomial&);	// Đơn thức đồng dạng
	bool sortMonomial(const Monomial&); // kiểm tra đơn thức nào đứng trước

	friend ostream& operator<<(ostream& oDev, Monomial& dt);
	friend class Polynom;

};


