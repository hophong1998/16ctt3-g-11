#pragma once

template<class T>
class QUEUE
{
	T *arr;
	int front, rear;
	int maxSize;
	int count;
public:
	//QUEUE();
	QUEUE(int size);
	QUEUE(const QUEUE<T>& oQUEUE);
	~QUEUE();

	bool isEmpty();
	bool isFull();
	bool enQueue(const T& key);

	T& frontValue();
	void deQueue();

};

template< class T>
QUEUE<T>::QUEUE(int size) {
	arr = new T[size];
	front = rear = 0;
	maxSize = size;
	for (int i = 0; i < maxSize; i++)
		arr[i] = '\0';
}

template <class T>
QUEUE<T>::QUEUE(const QUEUE<T> &oQUEUE) {
	if (arr != NULL)
		delete[] arr;
	front = oQUEUE.front;
	rear = oQUEUE.rear;
	maxSize = oQUEUE.maxsize;
	arr = new T[maxSize];
	for (int i = 0; i < maxSize; i++)
		a[i] = oQUEUE.arr[i];
}

template<class T>
QUEUE<T>::~QUEUE() {
	if (arr != NULL)
	{
		delete[] arr;
		front = 0;
		rear = 0;
		maxSize = 0;
	}
}

template<class T>
bool QUEUE<T>::isEmpty() {
	return count == 0;
}

template<class T>
bool QUEUE<T>::isFull() {
	return count == maxSize;
}

template<class T>
bool QUEUE<T>::enQueue(const T& key) {
	if (isFull()) {
		cout << "QUEUE OVERFLOW!" << endl;
		cin.get();
		return false;
	}
	else {
		arr[rear] = key;
		rear++;
		count++;
		if (rear == maxSize)  // xoay v�ng
			rear = 0;
	}
	return true;
}

template<class T>
void QUEUE<T>::deQueue() {
	if (isEmpty())
	{
		cout << "QUEUE is empty!" << endl;
		cin.get();
	}
	else {
		arr[front] = '\0';
		front++;
		count--;
		if (front == maxSize)
			front = 0;
	}
}

template<class T>
T& QUEUE<T>::frontValue() {
	if (isEmpty())
	{
		cout << "\nQUEUE is empty!" << endl;
		cin.get();
		exit(0);
	}
	else
		return arr[front];
}