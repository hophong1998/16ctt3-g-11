﻿#define _CRT_SECURE_NO_WARNINGS
#include "Monomial.h"
#include"Polynom.h"

#define tempBuff 20

//contructor
Monomial::Monomial()
{
	coeff = 0;
	var_num = 0;
	index = new float[tempBuff]; // cấp phát tạm thời
	var = new char[tempBuff]; // cấp phát tạm thời

}

//contructor
Monomial::Monomial(const Monomial& a)
{

	if (!var)
	{
		delete[] var;
		delete[] index;
	}
	coeff = a.coeff;
	var_num = a.var_num;
	index = new float[var_num];
	var = new char[var_num];
	for (int i = 0; i < var_num; i++) {
		var[i] = a.var[i];
		index[i] = a.index[i];
	}

}

//operator =
Monomial& Monomial::operator=(const Monomial& a) {
	if (this != &a)
	{
		if (!var)
		{
			delete[] var;
			delete[] index;
		}
		coeff = a.coeff;
		var_num = a.var_num;
		index = new float[var_num];
		var = new char[var_num];
		for (int i = 0; i < var_num; i++) {
			var[i] = a.var[i];
			index[i] = a.index[i];
		}
	}
	return *this;
}

//destructor
Monomial::~Monomial()
{
	if (var != NULL)
	{
		coeff = 0;
		var_num = 0;
		delete[] var;
		delete[] index;
	}
}

//phân tích đơn thức
Monomial& Monomial::ParseMonomial(char* s, int len) {
	int i = strcspn(s, "*^"); //tìm vị trí *^ đầu tiên
	if (i == 0x00000010) // TH không tìm thấy *^ trong chuỗi
		i = len;
	if (len == 1)  // trường hợp đơn thức 1 biến
	{
		if (isalpha(s[0]))
		{
			coeff = 1;
			var[var_num] = s[0];
			index[var_num] = 1;
		}
		return *this;
	}

	// pos: vị trí biến đầu tiên hoặc vị trí bắt đầu của hệ số

	// nếu đầu chuỗi có dấu + - thì vị trí biến đầu tiên sẽ là 1, ngc lại là 0
	int pos = (s[0] == '-' || s[0] == '+') ? 1 : 0;
	// nếu đầu chuỗi là dấu - thì hệ số âm, ngược lại là hệ số +
	coeff = s[0] == '-' ? -1 : 1;

	// xác định hệ số và vị trí biến đầu tiên
	if (isdigit(s[pos])) // sau pos là số
	{
		char* a = NULL;
		a = strtok(s + pos, "*"); // tách lấy hệ số
		double heso = atof(a);
		coeff *= heso;
		pos = i + 1; // vị trí biến đầu tiên nằm sau * đầu tiên
		if (pos > len) { // trường hợp hệ số tự do, ví dụ -12345, i sẽ = chiều dài chuỗi, pos=i+1 > len
			var[var_num] = 'a';
			index[var_num] = 0;
			var_num++;
			return *this;
		}
	}
	// kết thúc xác định hệ số và vị trí biến

	// xác định biến và số mũ tương ứng
	for (int j = pos; j < len; j = j + 2) {
		if (isalpha(s[j])) {
			if (s[j + 1] == '^') {
				var[var_num] = s[j];
				char* t = strtok(s + j + 2, "*+-");
				index[var_num] = atof(t);
			}
			else
			{
				var[var_num] = s[j];
				index[var_num] = 1;
			}
			var_num++;
			if (var_num == tempBuff) { // mảng danh sách các biến và mũ đầy
				char* new_var = (char*)realloc(var, 10 * sizeof(char));  // cấp phát thêm 10 vùng nhớ
				if (new_var != NULL)
					var = new_var;
				else
				{
					cout << "STACK OVERFLOW!\n";
					system("pause");
					exit(0);
				}
				float* new_index = (float*)realloc(index, 10 * sizeof(float)); // cấp phát thêm 10 vùng nhớ
				if (new_index != NULL)
					index = new_index;
				else
				{
					cout << "STACK OVERFLOW!\n";
					system("pause");
					exit(0);
				}
			}
		}
	}
	// sài không hết temBuff.
	index = (float*)realloc(index, var_num * sizeof(float)); // trả lại vùng nhớ dư thừa
	var = (char*)realloc(var, var_num * sizeof(char)); // trả lại lại vùng nhớ dư thừa
	return *this;
}

// Kiểm tra 2 đơn thức có đồng dạng hay không?
bool Monomial::operator==(const Monomial &T)
{
	if (T.index == NULL)
		return false;
	if (var_num != T.var_num)
		return false;
	for (int i = 0; i < var_num; i++) {
		if ((var[i] != T.var[i]) || (index[i] != T.index[i]))
			return false;
	}
	return true;
}

// kiểm tra xem đơn thức nào đứng trước
bool Monomial::sortMonomial(const Monomial& a) {
	if (*this == a)
		return true; // đơn thức đối tượng đứng trước đơn thức a
	if (a.var_num == 0)
		return false;
	int count = var_num < a.var_num ? var_num : a.var_num;
	for (int i = 0; i < count; i++) {
		if (index[i] > a.index[i])
			return false; // đơn thức đối tương đứng sau đơn thức a
		else if (index[i] == a.index[i]) // số mũ = nhau thì so sánh biến theo abc
		{
			if (var[i] > a.var[i])
				return false;
		}
	}
	return true;
}

// nhân đơn thức với số nguyên
Monomial& Monomial::operator*(float x)
{
	this->coeff *= x;
	return *this;
}

// chuẩn hóa đơn thức : sắp xếp theo thứ tự abc, cộng số mũ các biến giống nhau
void Monomial::Normalization() {
	for (int i = 0; i < var_num - 1; i++) {
		for (int j = i + 1; j < var_num; j++) {
			if (var[i] > var[j])
			{
				swap(index[i], index[j]);
				swap(var[i], var[j]);
			}
			if (var[i] == var[j]) {
				index[i] += index[j];
				for (int k = j; k < var_num - 1; k++) {
					var[k] = var[k + 1];
					index[k] = index[k + 1];
				}
				var_num--;
				var[var_num] = '\0';
			}
		}
	}
}

// rút gọn đơn thức: bõ đi các biến có số mũ =0
void Monomial::Simplify() {
	this->Normalization();
	for (int i = 0; i < var_num; i++) {
		if (index[i] == 0) {
			for (int j = i; j < var_num - 1; j++) {
				index[j] = index[j + 1];
				var[j] = var[j + 1];
			}
			var[var_num - 1] = '\0';
			var_num--;
			i--;
		}
	}
	index = (float*)realloc(index, var_num * sizeof(float));
	var = (char*)realloc(var, var_num* sizeof(char));
}

//ghi file
void Monomial::Output(ostream& oDev) {
	if (var_num == 0)
	{
		oDev << coeff;
		return;
	}
	if (coeff == -1)
		oDev << "-";
	if (coeff != 1 && coeff != -1)
		oDev << coeff << "*";
	for (int i = 0; i < var_num; i++) {
		oDev << var[i];
		if (index[i] != 1)
			oDev << "^" << index[i];
		if (i != var_num - 1) oDev << "*";
	}
}

//operator <<
ostream& operator<<(ostream& oDev, Monomial& dt) {
	if (dt.var_num == 0)
	{
		oDev << dt.coeff;
		return oDev;
	}
	if (dt.coeff == -1)
		oDev << "-";
	if (dt.coeff != 1 && dt.coeff != -1)
		oDev << dt.coeff << "*";
	for (int i = 0; i < dt.var_num; i++) {
		oDev << dt.var[i];
		if (dt.index[i] != 1)
			oDev << "^" << dt.index[i];
		if (i != dt.var_num - 1) oDev << "*";
	}
	return oDev;
}

//-----------------------------------------------

Monomial Monomial::operator*(const Monomial& T){
	Monomial R(*this);
	R.coeff *= T.coeff;
	if (R == T) {
		for (int i = 0; i < var_num; i++){
			R.index[i] *= 2;
		}
		return R;
	}
	float* new_index = (float*)realloc(R.index, (var_num + T.var_num) * sizeof(float));
	if (new_index != NULL)
		R.index = new_index;
	char* new_var = (char*)realloc(R.var, (var_num + T.var_num) * sizeof(char));
	if (new_var != NULL)
		R.var = new_var;
	for (int i = 0; i < T.var_num; i++){
		R.index[var_num + i] = T.index[i];
		R.var[var_num + i] = T.var[i];
	}
	R.var_num += T.var_num;
	R.Simplify();
	return R;
}

Monomial& Monomial::operator*=(const Monomial& T){
	coeff *= T.coeff;
	if (*this == T) {
		for (int i = 0; i < var_num; i++){
			index[i] *= 2;
		}
		return *this;
	}
	float* new_index = (float*)realloc(index, (var_num + T.var_num) * sizeof(float));
	if (new_index != NULL)
		index = new_index;
	char* new_var = (char*)realloc(var, (var_num + T.var_num) * sizeof(char));
	if (new_var != NULL)
		var = new_var;
	for (int i = 0; i < T.var_num; i++){
		index[var_num - 1 + i] = T.index[i];
		var[var_num - 1 + i] = T.var[i];
	}
	var_num += T.var_num;
	Simplify();
	return *this;
}