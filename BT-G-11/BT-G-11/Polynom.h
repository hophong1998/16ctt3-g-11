﻿#pragma once
#include"Monomial.h"

struct Mono { //struct đơn thức
	Monomial data;
	Mono* pNext;
	Mono* pPrev;
};

class Polynom  // đa thức
{
	Mono* pHead, *pTail;

public:
	Polynom();
	Polynom(const Polynom&);
	~Polynom();

	Mono* getMono(Monomial);

	bool addTail(Monomial);

	void Normalization();
	void Simplify();
	void Output(ostream& oDev);

	Polynom& ParsePolynom(string s); // tách chuỗi thành đa thức
	Polynom& operator=(const Polynom&);	 // operator =


	// nhân đa thức với 1 số
	Polynom& operator*=(float);
	Polynom operator*(float);


	// Phép cộng đa thức với đa thức
	Polynom& operator+=(const Polynom&);
	Polynom operator+(const Polynom&);

	Polynom& operator+=(const Monomial&);
	Polynom operator+(const Monomial&);
	//---------------------------------------

	// Phép trừ đa thức với đa thức
	Polynom& operator-=(const Polynom&);
	Polynom operator-(const Polynom&);

	Polynom& operator-=(Monomial);
	Polynom operator-(Monomial);

	//---------------------------------------
	// Phép nhân đa thức với đơn/đa thức
	Polynom operator*(const Monomial&);
	Polynom& operator*=(const Monomial&);
	Polynom operator*(const Polynom&);
	Polynom& operator*=(const Polynom&);


	friend ostream& operator<<(ostream& oDev, Polynom& dt);
	friend class Monomial;

};

