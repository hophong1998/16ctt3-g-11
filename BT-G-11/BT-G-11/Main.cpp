﻿#include"Polynom.h"
#include<fstream>
#include<string>

// xóa những khoảng trắng dư thừa trong chuỗi.
void StringNormalization(string& s) {
	for (int i = 0; i < s.length(); i++) {
		if (s[i] == ' ')
			s.erase(i, 1);
	}
}

void main() {
	string s1;
	ifstream fin("F1.txt", ios_base::in);
	getline(fin, s1);
	StringNormalization(s1);
	s1 += '+'; // thêm vào để khi tách đơn thức không bị sót đơn thức cuối cùng
	fin.close();
	string s2;
	ifstream fi("F2.txt", ios_base::in);
	getline(fi, s2);
	StringNormalization(s2);
	s2 += '+'; // thêm vào để khi tách đơn thức không bị sót đơn thức cuối cùng
	fi.close();
	string s;
	ifstream filein("F3.txt", ios_base::in);
	getline(filein, s);
	StringNormalization(s);
	s += '+'; // thêm vào để khi tách đơn thức không bị sót đơn thức cuối cùng
	filein.close();

	//==========================================================================
	Polynom f1, f2, f;
	// phân tích đa thức
	f1.ParsePolynom(s1);
	f2.ParsePolynom(s2);
	f.ParsePolynom(s);
	//==========================================================================

	//rút gọn & đơn giản
	f1.Simplify();
	cout << "F1= " << f1 << endl;
	f2.Simplify();
	cout << "F2= " << f2 << endl;
	f.Simplify();
	cout << "F= " << f << endl;

	//==========================================================================

	ofstream fout("F-result.txt", ios_base::out);
	fout << "F1:\n";
	f1.Output(fout);
	fout << "F2:\n";
	f2.Output(fout);
	fout << "F:\n";
	f.Output(fout);

	// cộng 2 đa thức
	Polynom f3 = f1 + f2;
	cout << "\nF3=F1+F2:\n" << f3 << endl << endl;
	fout << "F3:\n";
	f3.Output(fout);

	// trừ 2 đa thức
	Polynom f4 = f3 - f2;
	cout << "F4=F3-F2:\n" << f4 << endl << endl;
	fout << "F4:\n";
	f4.Output(fout);

	// nhân 2 đa thức
	Polynom f5;
	f5 = f1*f;
	f5.Simplify();
	cout << "F5=F*F1:\n" << f5 << endl << endl;
	fout << "F:\n";
	f5.Output(fout);

	fout.close();

	cin.get();
}